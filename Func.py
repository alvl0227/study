def accumulator():
    total = 0
    while True:
        value = yield total
        print('Got: {}'.format(value))

        if not value:
            break
        total += value

    print('Got: {}'.format(total))

