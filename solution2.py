import sys

digital_string = sys.argv[1]

if not digital_string.isdigit():
    print("введите число")
else:
    x = []
    for z in digital_string:
        x.append(int(z))
    print(sum(x))
    # Второй вариант
    # x = map(int, digital_string)
    #
    # print(sum(x))