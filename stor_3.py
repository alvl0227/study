import os
import argparse
import tempfile
import json

parser = argparse.ArgumentParser()
parser.add_argument("--key", type=str)
parser.add_argument("--val", type=str)
parser.parse_args()
args = parser.parse_args()
storage_path = os.path.join(tempfile.gettempdir(), 'storage.txt')


def write(key, value):
    with open(storage_path, 'w') as f:
        data = read()
        if key in data:
            data[key].append(value)
        else:
            data[key] = []
            data[key].append(value)
        f.seek(0)
        json.dump(data, f)


def read():
    with open(storage_path, 'r') as f:
        data = json.load(f)
    return data


if args.key and args.val:
    write(args.key, args.val)
elif args.key:
    print(", ".join([item for item in read()[args.key]]))