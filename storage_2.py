import os, sys, argparse, json, tempfile


def get_key(d, key):
    for v, k in d.keys():
        if k == key:
            return v


parser = argparse.ArgumentParser()
parser.add_argument('--key', dest='key_name')
parser.add_argument('--val', dest='value')

args = parser.parse_args()

my_dict = {}
storage_path = os.path.join(tempfile.gettempdir(), 'storage.txt')
if os.path.isfile(u'/tmp/storage.txt') == False:
    if args.key_name != None:
        if args.value != None:
            with open(storage_path, 'w') as f:
                json_dp = json.dump(my_dict.update([(args.key_name, args.value)]), f)
    else:
        print(None)
else:
    if args.key_name != None:
        if args.value != None:
            with open(storage_path, 'a') as f:
                json_dp = json.dump(my_dict.update([(args.key_name, args.value)]), f)

        else:
            with open(storage_path, 'r') as f:
                json_data = json.load(f)
                print(get_key(json_data, args.key_name))