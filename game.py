import random

number = random.randint(0, 5)

while True:
    answer = input("введите число от 0 до 100...")
    if not answer or answer == "exit":
        break

    elif not answer.isdigit():
        print("введите целое число")

    user_answer = int(answer)

    if user_answer < number:
        print("загаданное число больше")
    elif user_answer > number:
        print("загаданное число меньше")
    elif user_answer == number:
        print("вы угадали загаданное число")
