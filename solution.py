import sys

digital_string = sys.argv[1]

if not digital_string.isdigit():
    print("введите число")
else:
    n = int(digital_string)
    space_n = n - 1
    diez_n = n - (n - 1)

    for i in range(n):
        print((" " * (space_n - i)) + ("#" * (diez_n + i)))

# import sys
# # #
# # # num_steps = int(sys.argv[1])
# # #
# # # for i in range(num_steps):
# # #     print(" " * (num_steps - i - 1), "#" * (i + 1), sep="")